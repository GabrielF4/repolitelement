import {LitElement, html} from 'lit-element';

class FichaPersona extends LitElement {

    static get properties() {
        return {
            name : {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String},
            photo: {type: Object}
        };
    }

    constructor() {
        super ();

        this.name = "Prueba Nombre";
        this.yearsInCompany = 3;
        this.photo = {
            src: "./img/tf.jpg",
            alt: "Foto persona"
        };

        this.updatePersonInfo();
    }

    updated(changedProperties) {
        console.log("asd");
        changedProperties.forEach((oldValue, propName) =>  {
                console.log ("propiedad " + propName +" cambia valor, anterior era " + oldValue);
            }
        )

        if (changedProperties.has ("yearsInCompany") ) {
            console.log ("propiedad " + propName +" cambia valor, anterior era " + oldValue);
            updatePersonInfo ();
        }
        
    }

    render () {
        return html`
            <div>
                <label for="fname">Nombre completo</label>
                <input type="text" id="fname" name="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br />
                <label for="yearsInCompany">Años en la empresa</label>
                <input type="number" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
                <br />
                <input type="text" name="personInfo" value="${this.personInfo}" disabled ></input>
                <br />
                <img src="${this.photo.src}" height="200" width="200" alt="${this.photo.alt}">
            </div>
        `;
    }

    updateName (e) {
        console.log("updateName");
        this.name = e.target.value;
    }
    updateYearsInCompany (e) {
        console.log("updateYearsInCompany");
        this.yearsInCompany = e.target.value;
        this.updatePersonInfo ();
    }

    updatePersonInfo () {
        if (this.yearsInCompany >= 7) {
            this.personInfo = "lead";
        } else if (this.yearsInCompany >= 5) {
            this.personInfo = "senior";
        } else if (this.yearsInCompany >= 3) {
            this.personInfo = "team";
        } else {
            this.personInfo = "junior";
        }
    }
}

customElements.define('ficha-persona' , FichaPersona);