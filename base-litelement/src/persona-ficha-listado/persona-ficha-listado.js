import {LitElement, html} from 'lit-element';

class PersonaFichaListado extends LitElement {

    static get properties() {
        return {
            fname : {type : String} ,
            fyearsincompany : {type : Number},
            profile:{type:String},
            photo : {type : Object}
        };
    }

    constructor() {
        super ();
    }

    render () {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <div class="card">
            <img class="card-img-top" src="${this.photo.src}" alt="${this.photo.alt}" height="120" width="120"/>
            <div class="card-body">
                <h5 class="card-title">${this.fname}</h5>
                <p5>${this.profile}</p5>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">${this.fyearsincompany}</li>
                </ul>
            </div>
            <div class="card-footer">
                <button @click="${this.deletePerson}" class="btn btn-danger col-5"><strong>X</strong></button>
                <button @click="${this.moreInfo}" class="btn btn-info col-5 offset-1"><strong>Info</strong></button>
            </div>
        </div>
        <!--
        <label for="fname">Nombre</label>
        <input type="text" id="fname" name="fname" value="${this.fname}" />
        <label for="fyearsincompany">Años en la compañía</label>
        <input type="number" id="fyearsincompany" name="fyearsincompany" value="${this.fyearsincompany}" />
        -->
        <br/>
        `;
    }
    deletePerson (e) {
        console.log("deletePerson");
        console.log ("Vamos a borrar la persona "+this.fname);
        this.dispatchEvent(
            new CustomEvent (
                "delete-person",
                {
                    "detail" : {
                        name: this.fname
                    }
                }
            )
        );
    }
    moreInfo (e) {
        console.log("moreInfo "+this.fname);
        this.dispatchEvent(
            new CustomEvent (
                "info-person",
                {
                    detail : {
                        name: this.fname
                    }
                }
            )
        );
    }
}

customElements.define('persona-ficha-listado' , PersonaFichaListado);