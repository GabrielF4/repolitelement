import {LitElement, html} from 'lit-element';

class PersonaForm extends LitElement {

    static get properties() {
        return {
            person: {type: Object},
            editingPerson: {type: Boolean}
        };
    }

    constructor () {
        super ();
        this.resetFormData();
    }

    updated (changedProperties) {
        console.log ("changedProperties");
        console.log (changedProperties);
        console.log(this.person);
    }

    render () {
        console.log("RENDER "+this.person.profile);
        console.log (this.person);
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre Completo</label>
                        <input @change="${this.updateName}" .value="${this.person.name}" ?disabled="${this.editingPerson}" type="text" id="personFormName" class="form-control" placeholder="Nombre Completo" />
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea @change="${this.updateProfile}" .value="${this.person.profile}" class="form-control" placeholder="Perfil" rows="5" ></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input @change="${this.updateYearsInCompany}" .value="${this.person.yearsInCompany}" type="number" class="form-control" placeholder="Años en la empresa" />
                    </div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }

    updateName (e) {
        console.log ("updateName. El nombre es "+ e.target.value);
        this.person.name = e.target.value;
    }

    updateProfile (e) {
        console.log ("updateProfile. El perfil es "+ e.target.value);
        this.person.profile = e.target.value;
    }

    updateYearsInCompany (e) {
        console.log ("updateYearsInCompany. El tio lleva "+ e.target.value + " años en la compañía");
        this.person.yearsInCompany = e.target.value;
    }

    goBack (e) {
        console.log("goBack");
        e.preventDefault();
        this.resetFormData();

        this.dispatchEvent(new CustomEvent("persona-form-close",{}));
    }
    storePerson (e) {
        console.log("storePerson");
        e.preventDefault();

        this.person.photo = {
            src: "./img/tf.jpg",
            alt: "Foto persona"
        };


        console.log ("Los datos son: " + this.person.name + " " + this.person.profile + " " + this.person.yearsInCompany);
        
        this.dispatchEvent(new CustomEvent("persona-form-store",{
                detail : {
                    person: {
                        name: this.person.name,
                        profile: this.person.profile,
                        yearsInCompany: this.person.yearsInCompany,
                        photo: this.person.photo
                    },
                    editingPerson: this.editingPerson
                }
            })
        );
        // hago esto pero no estoy seguro..
        this.resetFormData();
    }
    resetFormData () {
        this.person = {};
        this.person.name = "";
        this.person.profile = "";
        this.person.yearsInCompany = "";

        this.editingPerson = false;
    }
}

customElements.define('persona-form' , PersonaForm);