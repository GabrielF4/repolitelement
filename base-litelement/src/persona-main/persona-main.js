import {LitElement, html} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js'
import '../persona-form/persona-form.js'

class PersonaMain extends LitElement {

    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }
    /*
    static get styles() {
        return css`
            :host {
                all: initial;
            }
        `;
    }
    */

    constructor() {
        super ();

        this.people = [
            {
                name: "Ellen Ripley",
                yearsInCompany : 1,
                photo: {
                    "src" : "./img/tf.jpg",
                    alt : "Ellen Ripley"
                },
                profile: "5Lorem ipsum Lorem ipsum."
            } ,{
                name: "Bruce Banner",
                yearsInCompany : 2,
                photo: {
                    "src" : "./img/tf.jpg",
                    alt : "Bruce Banner"
                },
                profile: "4Lorem ipsum Lorem ipsum."
            }, {
                name: "Eowin",
                yearsInCompany : 3,
                photo: {
                    "src" : "./img/tf.jpg",
                    alt : "Eowin"
                },
                profile: "3Lorem ipsum Lorem ipsum."
            }, {
                name: "Turunga Leela",
                yearsInCompany : 9,
                photo: {
                    "src" : "./img/tf.jpg",
                    alt : "Turunga Leela"
                },
                profile: "2Lorem ipsum Lorem ipsum."
            }, {
                name: "Jamie Lannister",
                yearsInCompany : 1,
                photo: {
                    "src" : "./img/tf.jpg",
                    alt : "Jamie Lannister"
                },
                profile: "1Lorem ipsum Lorem ipsum."
            }
        ];
        this.showPersonForm = false;
    }

    updated (changedProperties) {
        console.log ("updated de persona-main");
        if (changedProperties.has("showPersonForm")) {
            console.log ("Ha cambiado el valor de la propiedad ShowPersonForm en persona main");
            
            if (this.showPersonForm == true) {
                this.showPersonFormData ();
            } else {
                this.showPersonList ();
            }
        }
        if (changedProperties.has("people")) {
            console.log ("Ha cambiado el valor de la propiedad people en persona main");
            this.dispatchEvent(
                new CustomEvent (
                    "updated-people",
                    {
                        detail : {
                            people: this.people
                        }
                    }
                )
            );
        }
    }

    showPersonList () {
        console.log("show person list");
        this.shadowRoot.getElementById ("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById ("personaForm").classList.add("d-none");
    }
    showPersonFormData () {
        console.log("show person form data");
        this.shadowRoot.getElementById ("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById ("personaForm").classList.remove("d-none");
    }

    render () {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map(
                        person => html`<persona-ficha-listado 
                                @delete-person="${this.deletePerson}"
                                @info-person="${this.infoPerson}"
                                .fname=${person.name} 
                                fyearsincompany=${person.yearsInCompany} 
                                profile=${person.profile} 
                                .photo="${person.photo}"
                            >
                            </persona-ficha-listado>`
                    )}
                </div>
            </div>
            <div class="row">
                <persona-form 
                @persona-form-close="${this.personFormClose}" 
                @persona-form-store="${this.personFormStore}"
                id="personaForm" class="d-none border rounded border-primary"></persona-form>
            </div>
        `;
    }
    deletePerson (e) {
        console.log ("delete-person: persona-main");
        console.log (e.detail);

        this.people = this.people.filter (
            person => person.name != e.detail.name
        );
    }
    infoPerson (e) {
        console.log ("infoPerson: persona-main");
        console.log (e.detail);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );
        console.log(chosenPerson);
        this.shadowRoot.getElementById("personaForm").person = chosenPerson[0];
        this.shadowRoot.getElementById("personaForm").editingPerson = true;
        this.showPersonForm = true;
    }

    personFormClose (e) {
        console.log("personFormClose");
        this.showPersonForm = false;
    }
    personFormStore (e) {
        console.log("personFormStore");
        console.log(e.detail.person);

        if (e.detail.editingPerson == true) {
            console.log ("Actualizando persona " + e.detail.person.name);

            //let indexOfPerson = this.people.findIndex (person => person.name === e.detail.person.name);
            this.people.map (person => person.name === e.detail.person.name ? e.detail.person : person);
            /*
            if (indexOfPerson >= 0) {
                console.log ("persona encontrada "+this.people[indexOfPerson].name);
                this.people[indexOfPerson] = e.detail.person;
            }
            */

        } else {
            console.log ("Creando nueva persona " + e.detail.person.name);
            //this.people.push (e.detail.person);
            this.people = [...this.people, e.detail.person];
        }
        console.log("personFormStore terminado");
        this.showPersonForm = false;
    }
}

customElements.define('persona-main' , PersonaMain);