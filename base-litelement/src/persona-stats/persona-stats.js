import {LitElement, html} from 'lit-element';

class PersonaStats extends LitElement {
    static get properties () {
        return {
            people : {type:Array}
        };
    }

    constructor() {
        super ();
        this.people = {};
    }

    gatherPeopleArrayInfo (people) {
        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;
        return peopleStats;
    }
    
    updated (changedProperties) {
        console.log ("changedProperties persona-stats");
        console.log (changedProperties);
        
        if (changedProperties.has("people")) {
            console.log ("changedProperties persona-stats: ha cambiado people");
            
            let peopleStats = this.gatherPeopleArrayInfo (this.people);
            this.dispatchEvent(
                new CustomEvent (
                    "updated-people-stats",
                    {
                        detail : {
                            peopleStats: peopleStats
                        }
                    }
                )
            );
        }
    }

    
    
    render () {
        return html`
            <div>Persona stats</div>
        `;
    }
}

customElements.define('persona-stats' , PersonaStats );